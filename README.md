# 良心UI

## 概要
通过良心UI，做一套对IE6+支持的UI，整个框架为了支持工程样例，以nodejs + express + mongodb实现，可能考虑加入redis。

* [良心UI官网] http://lxui.hashiji.com/

## 工程框架目标

* 实现IE6+ 框架
* 服务器端替换灵活
* UI样例满足JK、QF、GZH等业务
* 测试先行

## 工程样例

* 提供一套完成的UI框架
* 增加FTP、压缩、解压缩实例（配置自动化事务处理）
* 日志跟踪
* 监控日志查询
* 邮件提醒

## 插件说明

* [实时日志监控官网] http://logio.org/

## 参考文章

* [FTP的断点续传] https://cnodejs.org/topic/4f5b47c42373009b5c04e9cb
* [文件夹监听] http://www.kuqin.com/shuoit/20150301/345016.html

## 期望实现的功能

* 监听文件夹
* 异步处理监听到的压缩文件，可视化进度管理
* 实现FTP上传（断点续传），解压缩、分类，放入输出文件夹，提供备份
* 对异常的处理
* 日志监听
