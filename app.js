/**
 * Created by zhangliu on 2016/7/1.
 */
var express = require('express');
var config = require('./config/config');
var logger = require('./config/logger');

var app = express();

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.listen(3010, function () {
    logger.info('web listening on port', config.port);
    logger.info('God bless love....');
    logger.info('You can debug your app with http://' + config.hostname + ':' + config.port);
    logger.info('');
});

module.exports = app;