/**
 * Created by zhangliu on 2016/7/1.
 */
/**
 * config
 */

var path = require('path');

var config = {
    debug: true,
    hostname: 'localhost',
    port: 3010
};

module.exports = config;